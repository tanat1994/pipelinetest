<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/users', function (Request $request) {
    return $request->user();
});